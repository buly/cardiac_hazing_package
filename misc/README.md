# Miscellaneous
---

<ins>Content:</ins>
- [Run Jupyter Notebook from server interactive session](#run-jupyter-notebook-from-server-interactive-session)
- [Ranger: Vim-key binding terminal File Browser](#ranger-vim-key-binding-terminal-file-browser)


## Run Jupyter Notebook from server interactive session

Locally, when we run `jupyter notebook`, 
the default browser will launch with link `localhost:8888` (new tab open if the browser is already running).
This give you access to the jupyter notebook home.

The idea of this section is to open a `jupyter` port on the server side 
then link it to local port, 
thus allowing you to work with `jupyer notebook` with the resource from NEF and IHUSUX (gpu, better cpu ...etc).

**<ins>NEF</ins>**

- Step 1 (server side): **Open oarsub interactive session with ssh key enabled**
	>Create a new ssh private key and copy to `~/.ssh/` on both local and server.
	>```console
	>user@nef-devel:~$ ssh-keygen -f ~/.ssh/id_nef
	>```
	>_cf. see quick start guide for how to copy file with `rsync`._
	>
	>Run interactive session with option `-ki ~/.ssh/id_nef` and note the node number `XX` in `nefgpuXX`
	>```console
	>user@nef-devel:~$  oarsub -ki ~/.ssh/id_nef -I -p "mem>8000 and gpu='YES' and gpucapability>='0.5'" -l /gpunum=1,walltime=12
	>user@nefgpuXX:~$
	>```
	>
	>Run activate conda environment and run jupyter notebook with option `--no-browser --port=8887`.
	>Noted the token in the output, it will be needed later.
	>```console
	>user@nefgpuXX:~$ conda activate env_nm
	>user@nefgpuXX:~$ jupyter notebook --no-browser --port=8887
	>```
	>
	>Leave interactive session and `jupyter` running.


- Step 2 (Local side): **Connecting to port=8887 to local port**

	>Add the following configuration to `~/.ssh/config` to direct connection to key `id_nef`.
	>You might need to create the `config` file.
	>``` console
	>Host *.neforward
	>    ProxyCommand ssh nef-frontal.inria.fr -W "$(basename %h .neforward):6667"
	>    LocalForward 8080 127.0.0.1:8080
	>    User oar
	>    Port 6667
	>    IdentityFile ~/.ssh/id_nef
	>```
	>
	>Link localhost:8880 (local) to localhost:8887 (server) with `ssh -N -L`. Replace `XX` with the node number.
	>``` console
	>user@local:~$ ssh -N -L localhost:8880:localhost:8887 nefgpuXX.neforward
	>```
	>
	>Open notebook session, in web browser using `localhost:8880`. 
	>Copy and paste the token and you're ready to
	>
	>![jupyter](../src/jupyter.png)

**<ins>IHU:</ins>**

Since, the session is not run via `oarsub`, 
we don't have to go through the ordeal with ssh key. 
The `jupyter`  session can directly connected vis `ssh -N -L`

- Step 1 (server side): 
> ```console
> user@IHUSUX003:~$ jupyter notebook --no-browser --port=8887
> ```

- Step 2 (local side): 
> Link localhosts.
> ```console
> user@local:~$ ssh -N -L localhost:8880:localhost:8887 user@ip_address
> ```
> Open `jupyter` session on web browser using `localhost:8880`

*..note: All the port numbers can be changed, so long as it stay 4 numbers and correctly paired.*

**<ins>Tip:</ins>**

Jupyter session can also be used as: 
- File browser
- File Transfer (Upload and Download files)
- Text Editor: Useful when debugging script on server. (Or you can just used `vim`.)
- (Pseudo) Terminal multipliers: you can open as many `interactive session` terminal as you.
- File Reader: Replace notebook with [Jupyter Lab](https://jupyterlab.readthedocs.io/en/stable/), 
then it can be used as image viewer, csv reader, ...etc.

![jupyter_tools](../src/jupyter_tool.png)

## Ranger: Vim-key binding terminal File Browser

[GitHub Link](https://github.com/ranger/ranger)

Ranger lets you browse and edit file on the terminal.
There are plenty of `ranger` cheat sheets online.
It might take sometime to get used to.
But it can make life easier on the terminal 
and is definitely a better alternative than spamming `ls` every 3 seconds.


```console
user@server:~$ pip install ranger-fm
suer@server:~$ ranger
```
![ranger](../src/ranger.png)


If you're not familiar with vim key binding, 
~what are you doing with your life~ 
the following are some basic commands you can do with ranger:

 Command | Key sequence |
 --- | ---
 Moving folder | Arrow keys or `h`:right `j`:down `k`:up and `l`:left
 Select/Unselect file | `space`
 Select All | `v`
 Unselect All | `uv`
 Copy highlight/selected file(s)/folder(s) | `yy`
 Cut highlight/selected file(s)/folder(s) | `dd`
 Paste file into current (middle) folder | `pp`
 Delete highlight/selected file(s)/folder(s) | `dD`
 Run shell command in current folder | `Ctrl+1`
 Run shell command on highlight/select file(s) | `Ctrl+2`

