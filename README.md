# Cardiac_Hazing_Package
---

A collection of short guides to get you through your first few hours on (calculation/linux) server (NEF and IHUSUX).

<ins>Content:</ins>
1. [Quick Start: Calculation Server 101](quick_start_calculation_server_101/)
2. [Miscellaneous](misc/)
	1. Link server jupyter to local
	1. Ranger: terminal file manager

*..note: The guide is written from personal experience trail and errors.*
