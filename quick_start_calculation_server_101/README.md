Quick Start Guide: Calculation Server 101
===

**Excalimer: This guide is made for python script calculation.
Nonetheless, it should be applicable for any other languages.**

<ins>Summary:</ins>
* [Get access to Server](#get-access-to-server)
* [Install `conda` and Required Packages](#install-conda-and-required-packages)
* [Transferring File with `rsync`](#transferring-file-with-rsync)
* [Running job with `oarsub`](#running-job-with-oarsub)
---

## Get Access to Server 

Create a private and public ssh key. (You might already have them, check in `~/.ssh/`)
>```console
>user@local:~$ ssh-keygen
>```
>Finish the key generation with default options (keep answering `yes` and `Enter`). 
At some point, you have to input a password of your choice.

This will create a public and private key (`~/.ssh/id_rsa.pub` and `~/.ssh/id_rsa`). 
Now, when strangers ask for your ssh key, send them the public key `id_rsa.pub`.


### 1. NEF

- Apply for nef account, using this link [https://nef-services.inria.fr/account/request](https://nef-services.inria.fr/account/request)

- ~Wait patiently for the email confirming, your account have been created.~ 
Spam `F5` key, on `zimbra.inria.fr` until you get the golden ticket (aka. a mail from noreply@inria.fr with the subject __Your NEF cluster account__)

- Spend some time on the official guide: 
  - User Guide: [https://wiki.inria.fr/ClustersSophia/User_Guide_new_config](https://wiki.inria.fr/ClustersSophia/User_Guide_new_config)
  - FAQ: [https://wiki.inria.fr/ClustersSophia/FAQ_new_config](https://wiki.inria.fr/ClustersSophia/FAQ_new_config)
  
- Connecting to NEF:
  - When connected to inria Ethernet or Wi-Fi:
  > ```console 
  >user@local:~$ ssh nef # type in the password of ~/.ssh/id_rsa
  >user@nef-devel:~$
  >```

  - When connected to external internet:
  >``` console
  >user@local:~$ ssh nef-frontal.inria.fr # to access nef fronend
  >user@nef-frontal:~$ ssh nef # to get to nef-devel
  >user@nef-devel:~$
  >```
  > There are some limitations on `frontal`,
  > so it is better just to get out of there quickly and work directly on `devel`.

- The distro on nef is `CentOS Linux release 7.4.1708`.
This does not matter, since you don't have super user access and
won't be able to use package management anyway.
But I just think it is cool to know.

To get out of nef, type `Ctrl+d` or just close the terminal.

..note: Nef session is for preping script/data/job/python evironment... etc. 
The ressource on nef is limited and shared between all users, 
thus no calculation or demanding process should be run at this point.

See [Running job with `oarsub`](#running-job-with-oarsub) to open calculation session, 
with higher calculation power.


### 2. IHUSUX003

- Contact the admin to create a user and to get access to `\data`
  - Currently the server is managed by: Julien Castelneau [julien.castelneau@ihu-liryc.fr](mailto:julien.castelneau@ihu-liryc.fr)

- Open connection with `netExtender` (You might need to install `NetExtender` first.)
  - Command line 
  > `netExtender -u "first_name.last_name" -p "password" -d ihu-liryc "vpn.ihu-lyric.fr"`
  - GUI 
  > Open `netExtenderGui` then fill in the details

	..note: You have to keep the netExtender session running to keep the connection to IHUSUX.
  
- Connecting to IHUSUX: 
  >```console 
  >~$ ssh username@ip_address
  >```
  >You get `username` and `ip_address` from Admin. Currently, the ip address is `10.33.35.60`

- The distro on `IHUSUX003` is `Ubuntu 18.04.3 LTS`
This does not matter, since you don't have super user access and
won't be able to use package management anyway.
But I just think it is cool to know.


..note: Theorically, the IHUSUX session is the same as nef, 
and calculations should be run via `oarsub`.
However, currently, there is a problem with the calculation `nodes`. 
Since you can already access all the GPUs, CPUs and REMs, 
it is possible to run your job directly.
Just pretend it is an interactive session with 2 GPUs and a lots of RAMs.

---

## Install `conda` and Required Packages

In Linux, most likely you already have `python` installed in `/bin/` 
(both version 2.x and 3.x). 
And you can start `pip install` right away.
However, because the original `python` is needed to run system packages 
and all the installed python applications.
It is safer to left this `python` untouched.

That is one of the main reason to use python environment (via environment manager, ex `conda`),
which allow user to install/uninstall local `python` and the required packages.
Using environment manager,

In this guide, we use `miniconda`, which is a lighter version.
The full flegde version is `Anaconda`, with pre-installed packages
and built-in GUI tools, including python editor ...etc.
It might be useful to install `Anaconda` locally,
however on the server, it is faster just to stick with `miniconda`.

- Download the latest version of miniconda linux (get the link from https://docs.conda.io/en/latest/miniconda.html and copy link from Python 3.~, linux 64bit installer.)
	>```console 
	>user@server:~$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	>```

- Install miniconda
	> ```console 
	>user@server:~$ bash Miniconda-latest-Linux-x86_64.sh
	>```
	>(Just accept every default config) Spam `yes` and `Enter`

  - (Optional) Remove install file
		> ```
		> user@server:~$ rm Miniconda-latest-Linux-x86_64.sh
		> ```

- Add `conda` to `PATH`
	>Try calling 
	>```console 
	>user@server:~$ conda
	>```
	>If you get error, you did something wrong. Call:
	>```console 
	>user@server:~$ ~/miniconda3/bin/conda init
	>```
	>`conda` should be in the `PATH` now.

- Create environment (cf. conda managing environment)
	> ```console 
	> user@server:~$ conda create --name env_name
	>```

- Access environment
	>```console
	>user@server:~$ conda activate env_name
	>```

- Installing Package
	> ```console 
	>user@server:~$ conda install package_name 
	>```
	> or
	>```console 
	>user@server:~$ pip install package_name
	>```
	>- The packages in main conda repo are usually optimized for conda environment and the packages currently installed.
	>- Search for "custom" conda repos if the package is not in the main repo (type: conda install package_name in google).
	>- Use `pip` as a last resort or to install custom package.


### Running `python` calculation

- Change to the `python` enviroment with required packages installed. (See above section)

- The most straight forward way to run python script is : 
	> ``` console 
	> user@local:~$ python /path/to/script.py --option argument
	> ```

- Another school of thinking is to run it from a package: (cf. google writing python package for details)
	> ```console 
	> user@local:~$ python -m package.module --option argument
	> ```
	> ..note: Option and Argument have to be coded in either script's or module's `__main__`.


---
## Transferring File with `rsync`

At a certain point, you might need to transfer file between your computer and the server.
There are multiple way to do this, one of the simplest ways is via `rsync`. 

Syntax: 
> `rsync [OPTIONS] /path/to/original/file /path/to/destination/file`

- NEF (internal internet): 
	> Upload (local-\>nef): 
	>```console
	>user@local:~$ rsync /path/to/local/file nef:/path/to/destination
	>```
	> Download (nef -> local): 
	>```console 
	>user@local:~$ rsync nef:/path/to/local/file /path/to/destination
	>```

- NEF (external internet): 
	>Upload (local-\>nef): 
	>```console 
	>user@local:~$ rsync /path/to/local/file nef-frontal.inria.fr:/path/to/destination
	>```
	>Download (nef -> local): 
	>```console
	>user@local:~$ rsync nef-frontal.inria.fr:/path/to/local/file /path/to/destination
	>```

- IHU: (connection opened via netExtender)
	> Upload (local-\>nef): 
	>```console 
	>user@local:~$ rsync /path/to/local/file user@ip_address:/path/to/destination
	>```
	> Download (nef -> local): 
	>```console 
	>user@local:~$ rsync user@ip_address:/path/to/local/file /path/to/destination
	>```


**<ins>Tip:</ins>**
- Use option `-r` to transfer recursively from folder destination 
- Create two folders for download and upload in both local and server side 
(perso. *Download folder:* `$HOME/pull` and *Upload folder:* `$HOME/push`) 
- Create a shell script, ex. `pull_nef.sh` and `push_nef.sh` with similar script: 
  >```
  ># pull_nef.sh
  >## Download files from nef $HOME/pull to $HOME/pull
  >## The script can be run with both internal and external inria network.
  >#!/bin/bash
  >rsync -avzru --exclude '*.' nef-frontal.inria.fr:$HOME/pull/ $HOME/pull/ || rsync -avzru --exclude '*.' --ignore-errors nef:$HOME/pull/ $HOME/pull 
  >```

  >```
  ># push_nef.sh
  >## Upload files from local $HOME/push to nef $HOME/push
  >## The script can be run with both internal and external inria network.
  >#!/bin/bash
  >rsync -avzru --exclude '*.' $HOME/push/ nef-frontal.inria.fr:$HOME/push/ || rsync -avzru --exclude '*.' nef:$HOME/push/ $HOME/push/ 
  >```
- Now, you can run the script (make sure the scripts is executable, using `chmod`), to download or upload files in `$HOME/pull` and `$HOME/push`.
- (Optional) Create soft link to local bin
  >```console
  >user@local:~$ ln -s $HOME/pull_nef.sh $HOME/.local/bin/pull_nef
  >```
  >Now, you can run `pull_nef.sh` from anywhere using: 
  >```console
  >user@local:~$ pull_nef
  >```

---
## Running job with `oarsub`

Go Read [Cluster FAQ](https://wiki.inria.fr/ClustersSophia/FAQ_new_config)


- Interactive mode: 
  >``` console
  >user@nef-devel:~$  oarsub -I -p "mem>8000 and gpu='YES' and gpucapability>='0.5'" -l /gpunum=1,walltime=12
  >```
  >
  >- `-I`: request session in interactive mode
  >- `-p`: propreties
  >>- `mem>8000`: request session with more than 8000 MB of RAM
  >>- `gpu='YES'`: request session with GPU
  >>- `gpucapability`
  >- `-l`: ressource list
  >>- `/gpunum=1` : request one GPU
  >>- `/walltime=12` : request session for 12h (after which the session will auto close.)

  Interactive mode open a terminal session, where you have access to the ressorces requested
  in `oarsub` argument.
  This mode is ideal for debugging, but you can still run your calculation here.
  However, be carefull se

- Job submission mode: 
  In the job submission mode, no terminal will be open, 
  and you can quite the connection to nef without ending the job.
  The stadard and error output will be written in 
  `OAR.XXXXXXXX.stdout` and `OAR.XXXXXXXX.stderr`.

  > Remove `-I` option and add job script.
  >``` console
  >user@nef-devel:~$  oarsub -I -p "mem>8000 and gpu='YES' and gpucapability>='0.5'" -l /gpunum=1,walltime=12 /path/to/executable_script
  >```
  >
  > To change the script to be executable (cf. google chmod calculator for more details):
  >```console
  >user@nef-devel:~$ chmod 700 /path/to/script
  >```

You can open to the running job session terminal (interactive or not) using: 
  > ```console 
  > user@nef-devel:~$ oarsub -C job_id
  >```
  > You can get job_id by calling: 
  > ```console
  > user@nef-devel:~$ oarstat | grep user
  > ```
  > ..note: It should be a given but `user` is your user name on the server.


**<ins>Writing Job script:</ins>**

*..note: As always, this is just one way of doing thing.*

```
## job_script.sh
#!/bin/bash
source activate conda_env  # activate conda environment
python /path/to/python_script.py # run python script
```

```
## submit_job.sh
#!/bin/bash
oarsub -I -p "mem>8000 and gpu='YES' and gpucapability>='0.5'" -l /gpunum=1,walltime=12 ~/job_script.sh
```

You can now submit job by executing `submit_job.sh`.

**<ins>Useful tools:</ins>**
- `htop`: Memory and CPUs usage
- `nvidia-smi`: Available GPUs and their usage
